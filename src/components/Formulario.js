import React, {useState} from "react";

const Formulario = ({setBusquedaL}) => {
    // 
    const [busqueda, setBusquedad] = useState({
        artista: '',
        cancion: ''
    });

    const [error, setError] = useState(false);
    const {artista, cancion} = busqueda;

    //funcion a cada input para leer contenido
    const actualizarState= e => {
        setBusquedad({
            ...busqueda,
            [e.target.name]: e.target.value
        })
    }
    // consultar api
    const buscarInformacion = e => {
        e.preventDefault();
        if(artista.trim()=== '' || cancion.trim()==='') {
            setError(true);
            return
        }

        setError(false);

        // TOdo bien, pasar al componente principal
        setBusquedaL(busqueda)
    }


    return (
        <div className="bg-info">
            <div className="container">
            {error ? <p className="alert alert-danger text-center p-2">Todos los campos son obligatorios</p> : null}
                <div className="row">
                    <form 
                        onSubmit={buscarInformacion}
                        className="col card text-white bg-transparent mb-5 pt-5 pb-2">
                        <fieldset>
                            <legend className="text-center">Buscador Letras Canciones</legend>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Artista</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="artista"
                                            placeholder="Nombre del artista"
                                            onChange={actualizarState}
                                            value= {artista}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Cancion</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="cancion"
                                            placeholder="Nombre de la cancion"
                                            onChange={actualizarState}
                                            value={cancion}
                                        />
                                    </div>
                                </div>
                            </div>
                            <button
                             type="submit"
                             className="btn btn-primary float-right"
                             >Buscar</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Formulario;
